# Shows

## Project description
This project is my very first one. 

It's aimed to automatically download new episodes of tv shows that are present in a particular folder in workspace (was aimed to work with NAS Synology. 

It worked for more than a year. 

**It does NOT work anymore** - I keep it visible to show how I started development. 

## When Shows came in my developer journey - Oct 2015  
This project is my very first IT project of my whole life. I actually started working on it right after my first Master's degree, and as I didn't even know what versionning a project was, it wasn't versioned... 
I versioned it later, in March 2016 or so. 

It explains why it's **anarchic**.

I don't even develop in it anymore, but I though it was great to share this project as I really started to learn "IT development" with this one. 

## What I learned with Shows
* The very basics in development
* Versionning
* Bash
* Python (a bit)
* REST APIs use - concept
* NAS Synology documentation/API use

### Shows : Not hosted anywhere - not a web app (the project)

# Personal cheat sheet

# Jusque là : 

 * Avant de commencer, le script regarde les DL dans series qui sont finis, et notifie (vis pushbullet) les DL pour un même fichier
 * Le script voit à quel épisode on en est et lequel il doit dl
 * trouve l'url de la page de l'épisode qui nous intéresse en 720p et hébergé sur uptobox
 * il affiche ce qu'il fait (lancer le prog avec | tee -a fich.log pour creer un fichier log de l'éxecution)
 * il supprime tous les .html/.txt téléchargés au début pour le traitement, de façon à laisser la place libre pour la prochaine exécution
 * il lance le dl de l'épisode n+1 dans le dossier "series/tv_shows/saison/tv_shows SNEN" pour chaque épisode (même si le dl est composé de plusieurs liens)
 * il place l'épisode dans un dossier du nom comme il faut (sans renommer le fichier lui même) mais peut suffir, A TESTER !
 * Il fait la différence entre un dossier de l'épisode N+1 qui est vide ou pas !
 * Il détecte quand TOUS les liens sont valides ou pas : 
     * Si valides -> Affiche: TOUS les liens sont valides
     * Si un DL est PAS en cours dans le dossier de réception -> ajoute TOUS les DL et affiche pour chaque lien: DL ajouté
     * Si un DL EST en cours dans le dossier de réception -> N'ajoute PAS les DL
     * Si PAS valide -> Affiche: Certains liens sont invalides, Affiche que les DL ne sont pas ajoutés.
     * POUR AMELIORER -> vérifier que TOUS les liens pour un meme DL sont ok ! (car pour le moment ne fait pas la différence)
 * Lance actualisation de kodi si il y a eu au moins 1 épisode de DL (sinon rien)
 * Supprime les taches de DL finies après les avoir notifié
 * Si les liens ne sont pas valides, il en essaie d'autres (par ordre de préférence -> 720p (du plus gros au plus petit), 1080p, HDTV) | il exclue les 480p
 * Sélectionne à qui il envoie des notifications de téléchargements terminés en fonction de ce qui est présent dans les .dat (gab ou ghys)
 * Supprime les fichiers "parasites" dus au DL
 * Changement dans la façon dont les notifications s'affichent -> titre de la series lu dans le dossier "series", num de la saison dans les dossiers saisons et le numero dans le dernier dossier
 * ATTENTION -> Rapidmoviez est protégé par Cloudflare (contre DDoS attaques) -> Utilisation de Cloudflare-scrape en python - dans bypass_cloudflare.py | Nouvelle session à chaque fois
 * Nouvelle façon de checker la validité des liens, plus robustes, regarde uniquement les codes HTTP des pages des liens (plus rapide aussi)
 * Adding a new way to notify -> if available, join the image of the episode from TMDB at the pushed notification.
 * Example of a notification : <img src="notifications_wo_img.png" width="340" hspace="20" align="left"><img src="notifications_m.png" width="340" align="left" hspace="20"><img src="notifications_n.png" width="340" hspace="20">


# Reste à faire : 

 * IMPORTANT ! -> Faire en sorte que le script connaisse la qualité de l'épisode n et regarde si meilleure qualité dispo pour le n+1 et DL si oui ! 
 * Pourrait aussi faire en sorte qu'il télécharge les sous-titres automatiquement
 * Pourrait faire en sorte que quand une nouvelle saison est dispo il DL pour la nouvelle saison
 * Pourrait aussi renommer les fichier qu'il a DL de la meme facon que le dossier parent (avec la bonne extension ATTENTION !)
 * Comprendre pourquoi Kodi ne télécharge pas l'image de la saison quand on change de saison...
 * Faire quelque chose de plus visuel pour voir quelles séries sont en "update" et pour voir à "qui elles appartiennent"
